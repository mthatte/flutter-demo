// Copyright 2016 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.outware.flutter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import io.flutter.view.FlutterMain;
import io.flutter.view.FlutterView;

public class ExampleActivity extends Activity {
    private static final String TAG = "ExampleActivity";

    private FlutterView flutterView;

    final SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {

            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

                float value = event.values[0];
                Log.d(TAG, "Accelerometer value changed: " + value);

                flutterView.sendToFlutter("accelerometerchange", String.valueOf(event.values[0]));
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FlutterMain.ensureInitializationComplete(getApplicationContext(), null);
        setContentView(R.layout.hello_services_layout);

        flutterView = (FlutterView) findViewById(R.id.flutter_view);
        flutterView.runFromBundle(FlutterMain.findAppBundlePath(getApplicationContext()), null);

        flutterView.addOnMessageListenerAsync("getLocation",
                new FlutterView.OnMessageListenerAsync() {
                    @Override
                    public void onMessage(FlutterView flutterView, String msg, FlutterView.MessageResponse messageResponse) {
                        onGetLocation(msg, messageResponse);
                    }
                }
        );

        flutterView.addOnMessageListener("startAccelerometer", new FlutterView.OnMessageListener() {
            @Override
            public String onMessage(FlutterView flutterView, String s) {
                boolean status = startAccelerometer();
                JSONObject reply = new JSONObject();
                try {
                    reply.put("status", status);
                } catch (JSONException e) {
                    Log.e(TAG, "JSON exception", e);
                }

                return reply.toString();
            }
        });

        flutterView.addOnMessageListener("stopAccelerometer", new FlutterView.OnMessageListener() {
            @Override
            public String onMessage(FlutterView flutterView, String s) {
                Log.d(TAG, "stopping accelerometer");
                stopAccelerometer();
                return null;
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (flutterView != null) {
            flutterView.destroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        flutterView.onPause();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        flutterView.onPostResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // Reload the Flutter Dart code when the activity receives an intent
        // from the "flutter refresh" command.
        // This feature should only be enabled during development.  Use the
        // debuggable flag as an indicator that we are in development mode.
        if ((getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0) {
            if (Intent.ACTION_RUN.equals(intent.getAction())) {
                flutterView.runFromBundle(intent.getDataString(),
                                          intent.getStringExtra("snapshot"));
            }
        }
    }

    private void onGetLocation(String json, final FlutterView.MessageResponse messageResponse) {
        String provider = "";
        try {
            JSONObject message = new JSONObject(json);
            provider = message.getString("provider");
        } catch (JSONException e) {
            Log.e(TAG, "JSON exception", e);
        }

        String locationProvider ="network";
        if (provider.equals("network")) {
            locationProvider = LocationManager.NETWORK_PROVIDER;
        } else if (provider.equals("gps")) {
            locationProvider = LocationManager.GPS_PROVIDER;
        } else {
        }

        String permission = "android.permission.ACCESS_FINE_LOCATION";
        Location location = null;
        LocationListener listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("Flutter demo android", location.getLatitude()+"  |  "+location.getLongitude());
                JSONObject reply = new JSONObject();
                try {
                    if (location != null) {
                        reply.put("latitude", location.getLatitude());
                        reply.put("longitude", location.getLongitude());
                    } else {
                        reply.put("latitude", 0);
                        reply.put("longitude", 0);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "JSON exception", e);
                }
                messageResponse.send(reply.toString());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if (checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestSingleUpdate(locationProvider, listener, Looper.myLooper());
        }

    }

    private boolean startAccelerometer(){
        final SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        final Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        if(accelerometer == null){
            return false;
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sensorManager.registerListener(sensorEventListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
            }
        }, 2);

        return true;
    }

    private void stopAccelerometer(){
        final SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorManager.unregisterListener(this.sensorEventListener);
    }


}
