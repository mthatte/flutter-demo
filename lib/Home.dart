import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'NavDrawer.dart';

class Home extends StatefulWidget{

@override
_HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home>{
  InputValue nameValue = new InputValue(text: '');
  GlobalKey<ScaffoldState> _scaffoldState = new GlobalKey<ScaffoldState>();
  Text _userName = new Text('');

  @override
  Widget build(BuildContext context){
    return new Scaffold(
          key: _scaffoldState,
          appBar: new AppBar(title: new Text('Greet Me!')),
          drawer: new NavDrawer(),
          body: new Column(
            children: <Widget>[
              new Text("Flutter Demo!", style: new TextStyle(color: new Color.fromRGBO(230,74,25, 1.0), fontSize: 25.0)),
              _userName,
              new Input(
                hintText: 'Enter your name here',
                labelText: 'Name',
                onChanged: (InputValue value){
                  nameValue = new InputValue(text: value.text);
                }
              ),
              new RaisedButton(
                child: new Text('greet me'),
                onPressed: (){
                  _scaffoldState.currentState.showSnackBar(new SnackBar(content: new Text(nameValue.text)));
                  setState((){
                      if(nameValue.text.trim() == ""){
                        _userName = new Text('');
                      }else{
                        _userName = new Text('Hello '+ nameValue.text);
                      }
                  });
                },
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceAround
          )
        );
  }
}
