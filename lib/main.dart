// Copyright 2015 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:math';

import 'NewsList.dart';
import 'Home.dart';
import 'Routes.dart';
import 'UserProfile.dart';
import 'FindMe.dart';

final Random random = new Random();

void main(){
   runApp(new MaterialApp(
                            title: "TechCruch",
                            routes: <String, WidgetBuilder>{
                                         Routes.HOME: (BuildContext context) => new Home(),
                                         Routes.USER_PROFILE : (BuildContext context) => new UserProfile(),
                                         Routes.TC_HOME: (BuildContext context) => new NewsList(),
                                         Routes.MY_LOCATION: (BuildContext context) => new FindMe(),
                                      },));

    // initialize a service binding and register a message handler callback
}
