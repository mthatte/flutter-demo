import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'NavDrawer.dart';
import 'dart:async';
import 'dart:io';

class FindMe extends StatefulWidget{

  @override
  _FindMeState createState() => new _FindMeState();
}

class _FindMeState extends State<FindMe>{
  GlobalKey<ScaffoldState> _scaffoldState = new GlobalKey<ScaffoldState>();

  double _latitude=0.0;
  double _longitude=0.0;
  String _accelerometer;
  bool _accelerometerStarted = false;

  @override
  void initState(){
    super.initState();
  //  _getLocation();
  }

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      key: _scaffoldState,
      appBar: new AppBar(title: new Text("Find Me")),
      drawer: new NavDrawer(),
      body: new Padding(
        padding: new EdgeInsets.all(16.0),
        child: new Block(
                children: <Widget>[
                  new Text("Latitude: $_latitude",),
                  new Text("Longitude: $_longitude"),
                  new Padding(
                    padding: new EdgeInsets.all(8.0),
                    child: new Center(
                        child: new Row(
                          children: <Widget>[
                          new RaisedButton(
                              child: new Text("Find Me!"),
                              onPressed: (){
                                _getLocation();
                              }
                          ),
                          new Padding(padding: new EdgeInsets.only(left: 16.0)),
                          new RaisedButton(
                              child: new Text("Show on map"),
                              onPressed: (){
                                  _showOnMap();
                              }
                          ),
                        ]),
                    ),
                  ),
                  new Padding(padding: new EdgeInsets.all(16.0)),
                  new Text("Accelerometer: $_accelerometer",),
                  new Padding(
                    padding: new EdgeInsets.all(8.0),
                    child: new Center(
                        child: new Row(
                          children: <Widget>[
                          new RaisedButton(
                              child: new Text(_accelerometerStarted ? "stop accelerometer" : "start accelerometer"),
                              onPressed: (){
                                if(_accelerometerStarted)
                                  StopAccelerometer();
                                else
                                  startAccelerometer();
                              }
                          ),
                        ]),
                    ),
                  ),
                ]
            ),
      ),
    );
  }


  Future<Null> _getLocation() async {
    if(Platform.isAndroid){
        final Map<String, String> message
          = <String, String>{'provider': 'network'};
        final Map<String, dynamic> reply
          = await HostMessages.sendJSON('getLocation', message);
        // If the widget was removed from the tree while the message was
        // in flight, we want to discard the reply rather than
        // calling setState to update our non-existant appearance.
        if (!mounted)
          return;
        setState(() {
          _latitude = reply['latitude'].toDouble();
          _longitude = reply['longitude'].toDouble();
        });
    }else{
      _scaffoldState.currentState.showSnackBar(new SnackBar(
                          content: new Text('Feature only implemented on Android!')));
    }
  }

  void _showOnMap(){
    if(this._latitude == null || this._longitude == null){
      _scaffoldState.currentState.showSnackBar(new SnackBar(
                          content: new Text('Get location by tapping on find me button first!')));
    }else{
      String url = "http://maps.google.com/maps?z=12&t=m&q=loc:$_latitude+$_longitude";
      UrlLauncher.launch(url);
    }
  }

  Future<Null> startAccelerometer() async {
    if(Platform.isAndroid){

      final Map<String, dynamic> reply
        = await HostMessages.sendJSON('startAccelerometer', "");

        String status="";
        if(reply['status']==true){
          status = "started";
          PlatformMessages.setStringMessageHandler("accelerometerchange", _handleAccelerometerEvent);
          setState((){
            //  _scaffoldState.currentState.showSnackBar(new SnackBar(content: new Text('got result')));
               _accelerometerStarted = true;
           });
        }else{
          status = "cannot be started";
        }

      _scaffoldState.currentState.showSnackBar(new SnackBar(content: new Text('Accelerometer $status')));

    }
  }

  Future<dynamic> _handleAccelerometerEvent(dynamic message) async {
    print("got msg: "+message);
   if (message == null)
     return;

  setState((){
    //  _scaffoldState.currentState.showSnackBar(new SnackBar(content: new Text('got result')));
       _accelerometer = message;
   });
 }

 Future<Null> StopAccelerometer() async {
   if(Platform.isAndroid){
      HostMessages.sendJSON('stopAccelerometer', "");
      setState((){
          _scaffoldState.currentState.showSnackBar(new SnackBar(content: new Text('Accelerator stopped')));
          _accelerometerStarted = false;
       });
   }
 }

}
