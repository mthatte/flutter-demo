import 'package:flutter/material.dart';
import 'utils.dart';
import 'Routes.dart';

class NavDrawer extends StatelessWidget{

  static String selectedRoute = Routes.HOME;
  Widget build(BuildContext context){
    return new Drawer(
      child: new Block(
        children:<Widget>[
              new UserAccountsDrawerHeader(currentAccountPicture: new CircleAvatar(child: new Text("J")),
                                            otherAccountsPictures: null,
                                            accountName: new Text("John Doe"),
                                            accountEmail: new Text("johndoe@email.com"),
                                            decoration: new BoxDecoration(backgroundColor: new Color.fromRGBO(230,74,25, 1.0)),
                                            onDetailsPressed: (){ Utils.showNamed(context, Routes.USER_PROFILE); }),
              new DrawerItem(child: new Text("Home"), onPressed: (){
                                                              selectedRoute = Routes.HOME;
                                                              Utils.showNamed(context, Routes.HOME);
                                                            },
                                                             selected: selectedRoute == Routes.HOME),
              new DrawerItem(icon: new ImageIcon(new AssetImage("assets/tc.png")),
                              child: new Text("TechCrunch top 10"),
                              onPressed: (){
                                selectedRoute = Routes.TC_HOME;
                                Utils.showNamed(context, Routes.TC_HOME);
                              },
                              selected: selectedRoute == Routes.TC_HOME),
              new DrawerItem(child: new Text("Find Me"), onPressed: (){
                                                              selectedRoute = Routes.MY_LOCATION;
                                                              Utils.showNamed(context, Routes.MY_LOCATION);
                                                            },
                                                             selected: selectedRoute == Routes.MY_LOCATION),
          ]
      )
    );
  }
}
