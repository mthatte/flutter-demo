import 'package:flutter/widgets.dart';
import 'NewsFeed.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef void ItemClicked(NewsFeed newsFeed);

class NewsListItem extends StatelessWidget{

  final NewsFeed newsFeed;

  NewsListItem(NewsFeed feed)
        : newsFeed = feed;

  @override
  Widget build(BuildContext context){
    return new ListItem(
      isThreeLine: true,
      dense: true,
      leading: null,
      title: new Text(newsFeed.title),
      subtitle: new Text(newsFeed.description),
      trailing: null,
      onTap: (){
        //print("does it work here?"+this.newsFeed.link);
          if(this.newsFeed.link != null){
            UrlLauncher.launch(this.newsFeed.link);
          }
      }
    );
  }
}
