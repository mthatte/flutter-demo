import 'package:flutter/material.dart';
import 'NavDrawer.dart';

class UserProfile extends StatelessWidget{
  final double _appBarHeight = 256.0;
  AppBarBehavior _appBarBehavior = AppBarBehavior.under;

  Widget build(BuildContext context){
    return new Scaffold(
      appBarBehavior: _appBarBehavior,
      appBar: _getAppBar(context),
      body: _getBody(context),
    );
  }

  Widget _getAppBar(BuildContext context){
      FlexibleSpaceBar flexibleSpaceBar = new FlexibleSpaceBar(
        title : new Text('John Doe'),
        background: new Stack(
          children: <Widget>[
            new Image.asset(
              'assets/user.jpg',
              fit: ImageFit.cover,
              height: _appBarHeight
            ),
            // This gradient ensures that the toolbar icons are distinct
            // against the background image.
            new DecoratedBox(
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                  begin: const FractionalOffset(0.5, 0.0),
                  end: const FractionalOffset(0.5, 0.30),
                  colors: <Color>[const Color(0x60000000), const Color(0x00000000)]
                )
              )
            )
          ]
        )
      );

      return new AppBar(
        expandedHeight: _appBarHeight,
        flexibleSpace: flexibleSpaceBar,
      );
  }

  Widget _getBody(BuildContext context){
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    return new Block(
              padding: new EdgeInsets.only(top: _appBarHeight + statusBarHeight),
              children: <Widget>[
                      _getSectionHeader("Phone"),
                      _getItem("+61-999-111-222"),
                      _getItem("+61-123-2345"),
                      _getItem("+61-333-222-222"),
                      new Divider(),
                      _getSectionHeader("Email"),
                      _getItem("johndoe@email.com"),
                      _getItem("thisisjohn@apple.com"),
                      new Divider(),
                      _getSectionHeader("Addresses"),
                      _getItem("111 Flinders street, Melbourne, VIC 3000"),
                      _getItem("999 Toorak road, Camberwell, VIC 3124"),
              ],
            );
  }

  Widget _getSectionHeader(String label){
    return new Padding(padding: new EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
                      child: new Text(label, style: new TextStyle(color: new Color.fromRGBO(230,74,25, 1.0), fontSize: 16.0, decorationStyle: TextDecorationStyle.solid)));
  }
  Widget _getItem(String label){
    return new Padding(padding: new EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
                      child: new Text(label));
  }
}
