import 'package:flutter/material.dart';

class Utils{
  static void showAlert(BuildContext context, int n){
      showDialog(context:context, child: new Dialog(child: new Padding(padding: new EdgeInsets.all(10.0), child: new Text("You selected item $n", textAlign: TextAlign.center))));
  }

  static void showNamed(BuildContext context, String routeName) {
      Navigator.popAndPushNamed(context, routeName);
    }
}
