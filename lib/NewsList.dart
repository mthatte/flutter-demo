import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/http.dart' as http;
import 'dart:convert';
import 'NewsFeed.dart';
import 'NewsFeedItem.dart';
import 'NavDrawer.dart';

class NewsList extends StatefulWidget{

@override
_NewsListState createState() => new _NewsListState();
}

class _NewsListState extends State<NewsList> {
  String URL = "https://newsapi.org/v1/articles?source=techcrunch&apiKey=9b390aa215934b3381f53d07f4353b9f";
  List<NewsFeed> items = new List<NewsFeed>();

  @override
  void initState(){
      super.initState();
      loadData();
  }

  @override
  Widget build(BuildContext context){
    return new Scaffold(
          drawer: new NavDrawer(),
          appBar: new AppBar(title: new Text('TechCrunch top news')),
          body: new Scrollbar(
              child: new MaterialList(
                  type: MaterialListType.threeLine,
                  padding: new EdgeInsets.symmetric(vertical: 4.0),
                  children: getListItemWidgets(this.items),
              )
            )
        );
  }

  List<Widget> getListItemWidgets(List<NewsFeed> feeds){
      List<Widget> widgets = new List<Widget>();
      for(int i=0; i<feeds.length; i++){
          widgets.add(new NewsListItem(feeds[i]));
      }
      return widgets;
  }

  void loadData() {
    // call the web server asynchronously
    http.get(URL).then((http.Response response){
      String jsonString = response.body;
      print(jsonString);
      parseJson(jsonString);
    });
  }

  void parseJson(String responseText){
    setState((){
        Map data = JSON.decode(responseText);
        List articles = data["articles"];
        items = new List<NewsFeed>();

        for(int i=0; i<articles.length; i++){
          String title = articles[i]["title"];
          String subtitle = articles[i]["description"];
          String link = articles[i]["url"];
          items.add(new NewsFeed(title, subtitle, link));
        }
    });
  }

}
