class NewsFeed{
  String title;
  String description;
  String link;

  NewsFeed(this.title, this.description, this.link);
}
